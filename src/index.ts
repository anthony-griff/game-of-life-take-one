import {GameOfLife} from './gameOfLife'

let game = new GameOfLife(40,40);

function randomColour(): Array<number>{
    var r = p.random(10,205);
    var g = p.random(10,205);
    var b = p.random(100,205);
    return [r,g,b]
}

var sketch = (p:p5) => {
  
    p.preload = () => {
        game.randomWorldGenerator();
    }
    
    p.setup = () => {
        const canvas = p.createCanvas(300, 300);
        canvas.parent("game-one")
        // p.frameRate(1)
    }
    
    p.windowResized = () => {
        //p.resizeCanvas(p.windowWidth, p.windowHeight);
    }

    p.keyPressed = () =>{
        if (p.keyCode === 32){
            game.randomWorldGenerator()
        };
    }

    p.draw = () => {
        p.background(0, 0, 0);

        // play the game
        game.play()

        // draw the board
        for (let cellState of game.boardIterator()){
            if (cellState.isAlive){
                p.fill(randomColour());
                let cellSize = 9;
                p.ellipse(cellState.cell.x * cellSize ,
                          cellState.cell.y * cellSize,
                          cellSize, cellSize);
            }
        }
    }
}

var p = new p5(sketch);