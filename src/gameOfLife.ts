/**
 * This file contains just the rules and the game loop.
 *
 * The board, is the game board, or the area of play
 *
 * For this example it is just boolean[][]
 *
 * */


// the cell is just the location x and y
export interface Cell {
    x: number
    y: number
}

// Cell State
interface ICellState {
  cell: Cell;
  isAlive: boolean;
  countOfNeighbours: number;
}

export class GameOfLife {
  ySize: number;
  xSize: number;
  public board: boolean[][];

  constructor(xSize: number, ySize: number) {
    this.xSize = xSize;
    this.ySize = ySize;
    this.board = this.createEmptyBoard();
  }

  createEmptyBoard(): boolean[][] {
    let board = [];
    for (var x = 0; x <= this.xSize; x++) {
      board[x] = [];
      for (var y = 0; y <= this.ySize; y++) {
        board[x][y] = false;
      }
    }

    return board;
  }

  /**
   * Nothing more than a random life generator
   */
  public randomWorldGenerator(): void {
    for (let cell of this.boardCell()) {
      this.board[cell.x][cell.y] = Math.round(Math.random()) ? true : false;
    }
  }

  /*
   * Just a simple way to set the cell to alive.
   * Not used?
   */
  public setCellAlive(cell: Cell): void {
    this.board[cell.x][cell.y] = true;
  }

  /*
   * For any cell, check it's current state on the board
   * Notice it check the board boundries as well.
   * Only cells on the board an checked.
   * If the cell is not on the board it's not alive
   */
  isAlive(cell: Cell): boolean {
    if (
      cell.x < 0 ||
      cell.y < 0 ||
      cell.y >= this.ySize ||
      cell.x >= this.xSize
    ) {
      return false;
    }
    return this.board[cell.x][cell.y];
  }

  /**
   * Using a generator here, it enables a loop to iterate over
   * a cells neighbours, encapsulating that in a generator
   * @param cell The current cell
   */
  *cellNeighbours(cell: Cell): IterableIterator<Cell> {
    let neighbours: Array<Cell> = [
      { x: -1, y: -1 },
      { x: 0, y: -1 },
      { x: 1, y: -1 },
      { x: -1, y: 0 },
      { x: 1, y: 0 },
      { x: -1, y: 1 },
      { x: 0, y: 1 },
      { x: 1, y: 1 },
    ];

    for (let neighbourCell of neighbours) {
      let cx = cell.x + neighbourCell.x;
      let cy = cell.y + neighbourCell.y;
      yield { x: cx, y: cy };
    }
  }

  countOfNeighbours(cell: Cell): number {
    let count: number = 0;
    for (let neighbourCell of this.cellNeighbours(cell)) {
      if (this.isAlive(neighbourCell)) count = count + 1;
    }
    return count;
  }

  /**
   * New life, determines if new life should be created, based on the
   * cellAlive state and the count of the cells alive neighbours
   * @param cellAlive The state of the cell alive or NOT
   * @param neighbourCount How many neighbours are alive
   * @returns true of false 
   */
  newLife(cellAlive: boolean, neighbourCount: number): boolean {
    // We only want the rules for new life, as that is all we will show on the new board

    if (cellAlive && (neighbourCount == 2 || neighbourCount == 3)) return true;

    if (!cellAlive && neighbourCount == 3) return true;

    return false;
  }

  *boardCell(funcX? : (x: number) => void, funcY? : (x: number, y: number) => void): IterableIterator<Cell> {
    // Return each cell on the board
    for (let x = 0; x <= this.xSize; x++) {
      if (funcX) funcX(x);
      for (let y = 0; y <= this.ySize; y++) {
        if (funcY) funcY(x, y);
        yield { x: x, y: y };
      }
    }
  }

  *boardIterator() {
    // returns the isAlive state of each cell
    // not sure if this is an iterator?
    for (let cell of this.boardCell()) {
      yield { cell, isAlive: this.isAlive(cell) };
    }
  }

  *world(): IterableIterator<ICellState> {
    // return each cell on the board
    // the cell state, isAlive and count
    for (let cell of this.boardCell()) {
      yield {
        cell,
        isAlive: this.isAlive(cell),
        countOfNeighbours: this.countOfNeighbours(cell),
      };
    }
  }

  play(): any {
    let newBoard = this.createEmptyBoard();

    for (let cellState of this.world()) {
      if (this.newLife(cellState.isAlive, cellState.countOfNeighbours)) {
        newBoard[cellState.cell.x][cellState.cell.y] = true;
      }
    }
    this.board = newBoard;
  }
}
