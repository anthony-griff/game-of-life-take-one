import { GameOfLife } from '../src/gameOfLife'

test('when creating the board', () => {
  let game = new GameOfLife(10, 10);
  game.createEmptyBoard()
  expect(game.board[1][1]).toBe(false);
});

test('when counting neighbours where none are alive', () => {
  // 0  0  0
  // 0  T  0
  // 0  0  0

  let game = new GameOfLife(10, 10);

  let cell = { x: 1, y: 1 };
  game.setCellAlive(cell);

  expect(game.isAlive(cell)).toBe(true);

  let count = game.countOfNeighbours(cell);

  expect(count).toBe(0)
});

test('rules for new life, alive cell 2 or 3 neighbours lives one', () => {
  // A cell with 2 or 3 neighbours should stay alive
  let game = new GameOfLife(10,10);
  let result = game.newLife(true, 2) 
  expect(result).toBe(true)
});

test('should rules for new life, dead cell with 3 neighbours, new life', () => {
  // A cell with 2 or 3 neighbours should stay alive
  let game = new GameOfLife(10,10);
  let result = game.newLife(false, 3) 
  expect(result).toBe(true)
});

test('when counting neighbours, should return 2', () => {

  // 1  1  0
  // 0  T  0
  // 0  0  0

  let game = new GameOfLife(10,10);

  game.setCellAlive({x:1,y:1});

  game.setCellAlive({x:1,y:2});

  const count = game.countOfNeighbours({x:2,y:2});
  expect(count).toBe(2)
});

