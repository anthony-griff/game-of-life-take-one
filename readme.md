# The Game of Life

This is an implementations of conway's game of life.

You can view it in action at this url : https://anthony-griff.gitlab.io/game-of-life-take-one/

Try pressing the space-bar to re-start.

## Install and setup

Set the correct version of node with :

>nvm use

Install packages ( you know the drill )

>npm i


## The tech-stack

* Tyescript
* Parcel
* Jest
* P5


## P5.js is a small canvas library

P5 is here : https://p5js.org/

P5 is a htmlcanvas wrapper, it's based of the processing project. It's main purpose is to allow you to draw/sketch to the screen. 

sketch.js is the main entry point for the project, a sketch is a function which the p5 system executes.

In this example the .draw method is execute over and over, the results are the drawing on the canvas. The speed/framerate can also be controlled.

All in all it is very much like the game loop in lua love of a pygame example.

There are a few addons/plugins libraries for p5 like p5.play which is used for sprites.

## Generators save the day

Writing the same loop over and over is boring. The game needs to loop over the cells in the board, in a number of different ways.

One generator function and do the trick!

## Jest - Test Cases

Install the Jest runner for vs code, this will run the test and indicate success or fail.


## Pallete

darkestColor = "#21212F"
darkColor = "#21212F"
lightColor = "#f5e3e0"
lightestColor = "#ff7357"
primaryColor = "#fefaaa"